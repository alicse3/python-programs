d={}
for i in map(int, input().strip().split(' ')):
    if i not in d:
        d[i]=1
    else:
        d[i]+=1
for k, v in sorted(d.items(), key=lambda x:x[1], reverse=True):
    print(k, end=" ")

#  Input: 999 2 3 1 2 2 3 10 1 1 1 1 1 100 300 100 3 5 9 1000 999 0
# Output: 1 2 3 999 100 10 300 5 9 1000 0 