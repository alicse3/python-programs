from tqdm import tqdm
import time

p = 1
for i in tqdm(range(10)):
    print(f"2 * {i} = {2*i}")
    time.sleep(p)
    if p > 5:
        p = 1
    else:
        p += 1
