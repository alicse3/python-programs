## Pytest instructions

To run tests:

```
pytest test_filename.py
```

To run tests with verbose output:

```
pytest test_filename.py -v
```

Another command to run tests:

```
py.test
```

For verbose output:

```
py.test -v
```

To run tests with matching option:

```
pytest -v -k "add"
```

Conditional match:

```
pytest -v -k "add or sub or div"
```

```
pytest -v -k "add and test"
```

To run markers:

```
pytest -v -m add
```

Run by disabling warnings:

```
pytest -v -m add --disable-warnings
```

To see skip tests with reason:

```
pytest -v -rsx
```

To show print statements output:

```
pytest -v -s
```

or

```
pytest -v --capture=no
```
