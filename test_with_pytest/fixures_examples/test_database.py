from database import DB
import pytest


@pytest.fixture(scope="module")
def conn():
    db = DB("test")
    db.connect()
    yield db
    db.close()


def test_select(conn):
    statement = "SELECT * FROM puppy;"
    result = conn.execute(statement)
    assert result == "{'name': 'puppy', 'age': 3}"


def test_insert(conn):
    statement = "INSERT INTO puppy VALUES('pup');"
    result = conn.execute(statement)
    assert result == "1 row inserted."
