class DB:
    def __init__(self, conn_str):
        self.conn_str = conn_str

    def connect(self):
        print("Connected to db.")
        pass

    def execute(self, statement):
        print(f"Executing {statement}")
        return "{'name': 'puppy', 'age': 3}" if statement.startswith("SELECT") else "1 row inserted."

    def close(self):
        print("DB connection closed.")
        pass
