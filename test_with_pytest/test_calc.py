import pytest
from calc import *


@pytest.mark.add
def test_add():
    assert add(10, 20) == 30
    assert add(31) == 30

    result = add("me", "you")
    assert type(result) is str
    assert "me" in result
    assert result == "meyou"

    print("Hello World!")


def test_sub():
    assert sub(50, 20) == 30


def test_mul():
    assert mul(10, 3) == 30


def test_div():
    assert div(600, 20) == 30


@pytest.mark.skip(reason="Testing")
def test_skip_1():
    assert add(10, 20) == 30
    assert sub(50, 20) == 30
    assert mul(10, 3) == 30
    assert div(600, 20) == 30


@pytest.mark.skipif(1 > 1, reason="Testing")
def test_skip_21():
    assert add(10, 20) == 30
    assert sub(50, 20) == 30
    assert mul(10, 3) == 30
    assert div(600, 20) == 30

# Parametrized


@pytest.mark.parametrize("num1, num2, result", [
    (1, 2, 3),
    ("me", "you", "meyou")
])
def test_parametrized(num1, num2, result):
    assert add(num1, num2) == result
