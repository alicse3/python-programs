class Employee():
    def __init__(self, e_name, e_age):
        self.__name = e_name
        self.__age = e_age

    @property
    def name(self):
        return self.__name

    @property
    def age(self):
        return self.__age

    @name.setter
    def name(self, ne_name):
        self.__name = ne_name


emp1 = Employee("Kitty", 3)
emp1.name = "Puppy"
# emp1.age = 33 # AttributeError: can't set attribute - because there is no setter for it
print(f"{emp1.name}, {emp1.age}")
