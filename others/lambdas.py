def mul(v):
    return lambda x: x * v


f = mul(2)
print(f(3))

f1 = mul(3)
print(f1(3))
