# Ex: 1
g_name = "Ali"
g_age = 25


def greet(name, age):
    print("Hello {}, you are {}".format(name, age))


greet(g_name, g_age)


# Ex: 2
def hi():
    global g_var
    g_var = "Hello"


hi()
print("Global %s" % g_var)
