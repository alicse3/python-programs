# Ex: 1
from math import inf

d = {
    "name": "Ali",
    "age": 25,
    "friends": ["Puppy", "Kitty"]
}

print("Dict length is: ", len(d))

for key, value in d.items():
    print("{}: {}".format(key, value))


print(d.get("name"))
print(d.keys())
print(d.values())
print(d.items())
print("age" in d)

d["name"] = "Alien"
d.update({"age": inf})
d.setdefault("unknown", "not known")

print(d)
print(d["unknown"])

# Ex: 2
print("***********************************")
nd = {
    "sun": "Sunday",
    "mon": "Monday",
    "tue": "Tuesday",
    "wed": "Wednesday",
    "thu": "Thursday",
    "fri": "Friday",
    "sat": "Saturday"
}

print(nd.items())
nd.pop("mon")
print(nd.items())
nd.popitem()
print(nd.items())
del nd["thu"]
print(nd.items())

# Ex: 3
print("***********************************")
week_days = {
    "sun": "Sunday",
    "mon": "Monday",
    "tue": "Tuesday",
    "wed": "Wednesday",
    "thu": "Thursday",
    "fri": "Friday",
    "sat": "Saturday"
}
for day in week_days:
    print(day, end=" ")

print()
for day in week_days:
    print(week_days[day], end=" ")
