import json

data = '{"name": "Puppy", "age": 25}'
py_obj = json.loads(data)
print(py_obj)
print(py_obj["name"])
print(py_obj["age"])


json_data = json.dumps(py_obj, indent=4)
print(json_data)

json_data_with_diff_seps = json.dumps(py_obj, indent=4, separators=(". ", " = "))
print(json_data_with_diff_seps)

json_data_with_key_sort = json.dumps(py_obj, indent=4, sort_keys=True)
print(json_data_with_key_sort)
