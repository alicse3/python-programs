"""
An iterator is an object that contains a countable number of values.

An iterator is an object that can be iterated upon, meaning that you can traverse through all the values.

Technically, in Python, an iterator is an object which implements the iterator protocol,
which consist of the methods __iter__() and __next__().
"""


class MyNumbers:
    def __iter__(self):
        self.n = 1
        return self

    def __next__(self):
        if self.n <= 10:
            v = self.n
            self.n = v + 1
            return v
        else:
            raise StopIteration


my_nums = MyNumbers()
my_iter = iter(my_nums)
for i in range(10):
    print(next(my_iter))

print(next(my_iter))
