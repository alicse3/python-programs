class Animal:
    def __init__(self, name):
        self.name = name
        print("Animal constructor called...")

    def print_details(self):
        print("Name is:", self.name)


class Dog(Animal):
    def __init__(self, name, age):
        super().__init__(name)
        self.age = age
        print("Dog constructor called...")

    def print_details(self):
        print("Name is: {} and age is: {}".format(self.name, self.age))


a = Animal("Unknown")
a.print_details()

d = Dog("Puppy", 3)
d.print_details()
