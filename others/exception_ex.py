try:
    # print(x)
    # 10/0
    print("Hello there...")
except NameError as ne:
    print(ne)
except Exception as e:
    print(e)
else:
    print("No exception!")
finally:
    print("No matter what, I will be executed.")
