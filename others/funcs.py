# Ex: 1
def var_args(*args):
    for v in args:
        print(v, end=" ")
    print()


var_args(1, 2, 3, 4, 5)


# Ex: 2
def var_kwargs(**kwargs):
    for k, v in kwargs.items():
        print(k, ": ", v)


var_kwargs(name="Ali", age=25)


# Ex: 3
def default_val(v = 3):
    print(v**v)


default_val(10)
default_val()


# Ex: 4
def list_as_arg(l):
    for v in l:
        print(v, end=" ")
    print()


list_as_arg([5, 4, 3, 2, 1])
