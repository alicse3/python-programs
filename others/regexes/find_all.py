from data_helper import *
import re

result = re.findall("[a-z]", txt)
print(result)

result_1 = re.findall("\d", txt_1)
print(result_1)

result_2 = re.findall("H...o", txt)
print(result_2)

result_3 = re.findall("^Hello", txt)
if result_3:
    print("Yes, {} starts with: {}".format(txt, "Hello"))
else:
    print("No, {} doesn't starts with: {}".format(txt, "Hello"))
