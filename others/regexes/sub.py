from data_helper import *
import re

print("Before:", txt_1)
result = re.sub("My", "Ali", txt_1)
print("After:", result)

print("Before:", txt)
result_1 = re.sub("l", "k", txt, 1)
print("After:", result_1)
