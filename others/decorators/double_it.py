def x(func):
    def y(*args):
        return func(*args) * 2

    return y


@x
def double(n):
    return n * n


print(double(10))
