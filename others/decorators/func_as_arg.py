msg = "HeLLo WorLD!"


def up(arg): return arg.upper()


def low(arg): return  arg.lower()


def cap(arg): return  arg.capitalize()


def greet(func): print(func(msg))


greet(up)
greet(low)
greet(cap)
