def my_dec(func):
    def wrapper(*args):
        func(*args)
        func(*args)
    return wrapper


@my_dec
def test():
    print("Hello World!")


test()
