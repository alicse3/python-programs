def hello(func):
    def hi():
        print("Before")
        func()
        print("After")
    return hi


@hello
def me():
    print("Me me me!")


me()
