def hmm(func):
    def yup():
        print("Before from yup")
        func()
        print("After from yup")
    return yup


def hello(func):
    def hi():
        print("Before")
        func()
        print("After")
    return hi


@hello
@hmm
def one():
    print("One")


one()

# Output
'''
Before
Before from yup
One
After from yup
After
'''