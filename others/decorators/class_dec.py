class Decorator:
    def __init__(self, func):
        self.func = func

    def __call__(self):
        msg = self.func()
        return msg.upper()


@Decorator
def greet():
    return "Hello World!"


print(greet())
