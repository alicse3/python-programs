def one(x):
    def two(y):
        return x + y
    return two


o = one(10)
t = o(20)
print(t)