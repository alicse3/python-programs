def my_dec(func):
    def w(m, *args):
        return func(*args) * m
    return w


@my_dec
def z():
    return 10


print(z(10))
