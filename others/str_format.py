# Ex: 1
name = "Ali"
age = 25
sentence = "Name is {} and age is {}"
print(sentence.format(name, age))

# Ex: 2
# We can use index numbers {0} to be sure the arguments are placed in the correct placeholders:
new_sentence = "Name is {1} and age is {0}"
print(new_sentence.format(age, name))


