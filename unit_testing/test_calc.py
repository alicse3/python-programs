import unittest
from calc import *


class TestCalc(unittest.TestCase):

    def test_add(self):
        self.assertEqual(add(100, 200), 300)
        self.assertEqual(add(0, 1), 1)
        self.assertEqual(add(-99, 9), -90)
        self.assertEqual(add(-1, 1), 0)
        self.assertEqual(add(1000, 10**2*10+10**3), 3000)
        self.assertEqual(add(-1000, -1), -1001)

    def test_sub(self):
        self.assertEqual(sub(10, 20), -10)
        self.assertEqual(sub(-10, -20), 10)
        self.assertEqual(sub(0, -1), 1)
        self.assertEqual(sub(1000, 99), 901)
        self.assertEqual(sub(-1, 11), -12)

    def test_mul(self):
        self.assertEqual(mul(10, 20), 200)
        self.assertEqual(mul(-10, -20), 200)
        self.assertEqual(mul(0, -1), 0)
        self.assertEqual(mul(1000, 99), 99000)
        self.assertEqual(mul(-1, 11), -11)

    def test_div(self):
        self.assertEqual(div(100, 50), 2)
        self.assertEqual(div(10, 2), 5)
        self.assertEqual(div(0, 10), 0)
        self.assertEqual(div(-10, 10), -1)

        self.assertRaises(ValueError, div, 10, 0)

        with self.assertRaises(ValueError):
            div(1, 0)


if __name__ == '__main__':
    unittest.main(verbosity=2)
