import unittest
from person import Person


class TestPerson(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("=> setUpClass")

    @classmethod
    def tearDownClass(cls):
        print("=> tearDownClass")

    def setUp(self):
        print("=> setUp")
        self.puppy = Person("Puppy", 3)
        self.kitty = Person("Kitty", 2)

    def tearDown(self):
        print("=> tearDown")

    def test_name(self):
        print("=> test_name")
        self.assertEqual(self.puppy.name, "Puppy")
        self.assertEqual(self.kitty.name, "Kitty")

    def test_age(self):
        print("=> test_age")
        self.assertEqual(self.puppy.age, 3)
        self.assertEqual(self.kitty.age, 2)

    def test_details(self):
        print("=> test_details")
        self.assertEqual(str(self.puppy), "Name is Puppy and age is 3.")
        self.assertEqual(str(self.kitty), "Name is Kitty and age is 2.")


if __name__ == '__main__':
    unittest.main(verbosity=2)
