tcs = int(input().strip())
d = {0: 6, 1: 2, 2: 5, 3: 5, 4: 4, 5: 5, 6: 6, 7: 3, 8: 7, 9: 6}

def print_result(c, v):
    while c>0:
        print(v, end="")
        c-=1

def get_sticks_count(s):
    c = 0
    for i in s:
        c += d[int(i)]
    return c

while tcs > 0:
    s_num = input().strip()
    s_count = get_sticks_count(s_num)
    if (s_count-3)%2 == 0:
        print_result(1, 7)
        print_result((s_count-3)/2, 1)
    else:
        print_result((s_count)/2, 1)
    print()
    tcs -= 1
