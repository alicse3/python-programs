s=input()
c=0
for i in s:
    if i=="*":
        c+=1
    else:
        if c==0:
            print(i, end="")
        else:
            print ("*", i, sep="", end="")
            c=0
if c!=0:
    print("*")

# Sample Input/Output
# Input: ****a**bcd***e**f***ghi*****j******
# Output: *a*bcd*e*f*ghi*j*