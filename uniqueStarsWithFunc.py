def solve(s):
    c=0
    result=""
    for i in s:
        if i=="*":
            c+=1
        else:
            if c==0: 
                result+=i
            else:
                result+="*"+i
                c=0
    if c!=0:
        result+="*"
    return result

s=input()
print(solve(s))


# Sample Input/Output
# Input: ****a**bcd***e**f***ghi*****j******
# Output: *a*bcd*e*f*ghi*j*