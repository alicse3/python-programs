import click


@click.command()
@click.option('--name', '-n', help="Some name", default="Alien")
@click.option('--age', '-a', help="Some age", default="2525 years")
@click.option('--message', '-m', required=True)
def main(name, age, message):
    print(f"The message is {message}.")
    print(f"Hello World! My name is '{name}' and age is '{age}'.")


if __name__ == "__main__":
    main()


"""
Command: 
python3.9 click_example/ex.py --name "Hmm" -a 25 -m "mandatory"

Output: 
The message is mandatory.
Hello World! My name is 'Alien' and age is '2525 years'.
"""
