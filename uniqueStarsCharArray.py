s=input()
counter, indexTracker = 0, 0
for i in range(len(s)):
    if s[i] == '*':
        if counter == 0:
            s[indexTracker] = s[i]
            indexTracker += 1
        counter += 1
    else:
        s[indexTracker] = s[i]
        indexTracker += 1
        counter = 0
print(s[:indexTracker])


# Sample Input/Output
# Input: ****a**bcd***e**f***ghi*****j******
# Output: *a*bcd*e*f*ghi*j*

# Sample Char Array Input
# input1  = ['a', '*', '*', 'b', 'c', 'd', '*', '*', '*', 'e', '*', '*', 'f', '*', '*', '*', 'g', 'h', 'i', '*', '*', '*', '*', '*', 'j', '*', '*', '*', '*', '*', '*']
# output1 = ['a', '*', 'b', 'c', 'd', '*', 'e', '*', 'f', '*', 'g', 'h', 'i', '*', 'j', '*']
# input2  = ['*', '*', 'a', '*', '*', 'b', 'c', 'd', '*', '*', '*', 'e', '*', '*']
# output2 = ['*', 'a', '*', 'b', 'c', 'd', '*', 'e', '*']
